
from csv_handling import *
from utils import *
import re

key_list= ["half_wave_rect_10_uF","half_wave_rect_100_uF","half_wave_rect_1000_uF","full_wave_rect_1000_uF" ]

def plot_data_fromsearchkey(key,ax):
    data = get_data_fromsearchkey(key,dtype="float")
    div = 2 if "full" in key else 1
    capacity = int(re.search(r'(?<=_)\d+(?=_uF)', key).group()) # in uF
    mod_wave = "full" if "full" in key else "half"
    label = f"{mod_wave} wave : {capacity} uF"
    
    plotdata(ax[0],data[0],data[1],label=label)
    plotdata(ax[1],data[0],data[2])
    plotdata(ax[3],data[0], data[2]/data[1]*100)

    
    freq = 50
    calculated_ripple = data[0]/div/capacity/freq*1e6
    plotdata(ax[2],data[0], data[2]/calculated_ripple)


def plot_rms_ripple_voltage_keylist(keylist):
    fig,ax= plt.subplots(4,1,sharex=True)
    
    titles = ["rms voltage","Ripple Voltage","Ripple voltage measured/calculated","percentage Ripple Voltage"]
    for (title,axis) in zip(titles,ax):
        axis.set_ylabel("Voltage (v)")
        axis.set_title(title)  
    ax[2].set_ylabel("rel Voltage (%)")  
    ax[3].set_ylabel("rel Voltage (%)")  
    ax[-1].set_xlabel("Current (A)")

    for key in keylist:
        plot_data_fromsearchkey(key,ax)
    
    plt.tight_layout()
    count= len(keylist)
    ncol = int(count/int((count/5+1))+1)
    ax[0].legend(loc='lower left', bbox_to_anchor=(0,-0.4,1,-0.25),mode="expand", ncol=ncol)
    plt.show()

plot_rms_ripple_voltage_keylist(key_list)