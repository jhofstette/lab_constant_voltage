import os
import numpy as np
import matplotlib.pyplot as plt

class NP_2D_DictWrapper:
    """
    intended to be used with numpy.genfromtxt, names=true
    overloads the [  ] operator to either be able to use headername to access a column
    or the column index
    to get row use get_row(index)
    get keys with keys()
    """
    def __init__(self, data):
        self.data = data
        self.keys()
    
    def get_row(self, index):
        return self.data[index]
    
    def column_count(self):
        return len(self.data.keys())
    
    def keys(self):
        return self.data.dtype.names
    
    def __str__(self):
        return str(self.data)

    def __getitem__(self, key):
        # Check if key is a string (header name)
        if isinstance(key, str) or isinstance(key, slice):
            return self.data[key]
        elif isinstance(key, int):
            return self.data[self.data.dtype.names[key]]
        else:
            raise TypeError("Key must be either a string (column name),an integer (column index) or a slice")


FOLDER_DATA_DEF = "csv_data"
FOLDER_FIT_DEF = "saved_plots"

def read_csv_to_dict(filename,FOLDER_DATA = FOLDER_DATA_DEF,header=True,dtype=None):
    """
    returns wrapped data from numpy genfromtxt with class :class:`NP_2D_DictWrapper`
    """
    if FOLDER_DATA is None:
        FOLDER_DATA = FOLDER_DATA_DEF
    path = f"{FOLDER_DATA}/{filename}"
    if filename=="":
        return None
    data = np.genfromtxt(path, delimiter=';', dtype=dtype, names=header, encoding=None)
    if header:
        data = NP_2D_DictWrapper(data)
    return data


def save_current_plot(name, FOLDER_FIT = FOLDER_FIT_DEF):
    if not os.path.exists(FOLDER_FIT):
            try:
                os.makedirs(FOLDER_FIT)
            except OSError as e:
                print(f"Error creating folder: {e}")
    filename = f"{FOLDER_FIT}/{name}.png"
    plt.savefig(filename)

def get_filename(par,FOLDER_DATA = FOLDER_DATA_DEF) :
    """
    tries to get a filename containing the keyword in the
    """
    data_list = os.listdir(FOLDER_DATA)
    if(isinstance(par, int)) :
        return data_list[par]
    else:
        for data in data_list:
            if par in data:
                return data
    print(f"didnt find: {par}")
    return ""

def get_data_fromsearchkey(searchkey,header=True,dtype=None) :
    try:
        filename = get_filename(searchkey)
        result = read_csv_to_dict(filename,header=header,dtype=dtype)
        return result
    except Exception as e:
        print("Error:", e)


def least_significant_digit_power(number_str):
    """
    Extracts the power of the least significant digit from a number in string format.

    Args:
    number_str (str): The number in string format.

    Returns:
    float: The power of the least significant digit.
    """
    lsd_power=0
    # Handle scientific notation by splitting at 'e' or 'E'
    if 'e' in number_str or 'E' in number_str:
        base_str, exponent_str = number_str.lower().split('e')
        number_str=base_str
        lsd_power += int(exponent_str)

    # Regular decimal notation
    if '.' in number_str:
        decimal_part = number_str.split('.')[1]
        lsd_power -= len(decimal_part)
    
    return lsd_power

