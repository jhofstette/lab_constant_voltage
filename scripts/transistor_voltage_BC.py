from csv_handling import *
import matplotlib.pyplot as plt

def plotdata(x,y,label=None,zorder=1):
    return plt.plot(x,y,label=label,marker='o', linestyle='-', linewidth=0.8,markersize=3,zorder=zorder)

key= "source_150_Zener_50"
data1 = get_data_fromsearchkey("buffer_1k",dtype="float")
data2 = get_data_fromsearchkey("buffer_10k",dtype="float")
plotdata(data1[0],data1[0]-data1[1],label="1k potentiometer")
plotdata(data2[0],data2[0]-data2[1],label="10k potentiometer")
plt.xlabel("base voltage(V)")
plt.ylabel("base-collector voltage(V)")
plt.title("voltage base-collector")
plt.legend()
plt.show()