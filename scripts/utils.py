from csv_handling import *
import numpy as np
import matplotlib.pyplot as plt
import re

def get_zener_data(key):
    pattern = r'([^_\d]+)_(\d+)(_(\d))?V_(\d+)'
    match = re.search(pattern, key)

    if not match:
        return None,None,None

    firstkey = match.group(1)
    voltage = int(match.group(2))
    if match.group(4):
        voltage+=int(match.group(4))/10
    resistance=int(match.group(5))
    return resistance,voltage,firstkey

def plotdata(axis,x,y,label=None):
    axis.plot(x,y,label=label,    marker='o', linestyle='-', linewidth=0.8,markersize=3,)

U_rms = 18.66
def plot_data_fromsearchkey(key,ax,try_zener_shift=False):
    data = get_data_fromsearchkey(key,dtype="float")
    current = data[0] 
    res,volt,firstkey = get_zener_data(key)
    label = key
    if not(res is None):
        if try_zener_shift:
            current +=(U_rms-volt)/res
        label = f"{firstkey}: {volt} V, {res} $\Omega$"
    
    plotdata(ax[0],current,data[1],label=label,)
    plotdata(ax[1],current,data[2],)
    plotdata(ax[2],current, data[2]/data[1]*100,)



def plot_rms_ripple_voltage_keylist(keylist,try_zener_shift=False,show_abs_ripple=True):

    height_ratios=[1, 1,1]
    if not show_abs_ripple:
        height_ratios=[1,0,1]
    fig,ax= plt.subplots(3,1,sharex=True,height_ratios=height_ratios)
    
    titles = ["rms voltage","Ripple Voltage","percentage Ripple Voltage"]
    for (title,axis) in zip(titles,ax):
        axis.set_ylabel("Voltage (v)")
        axis.set_title(title)  

    ax[2].set_ylabel("rel Voltage (%)")  
    ax[-1].set_xlabel("Current (A)")

    for key in keylist:
        plot_data_fromsearchkey(key,ax,try_zener_shift=try_zener_shift)

    if not show_abs_ripple:
        ax[1].set_visible(False)
    plt.tight_layout()
    plt.subplots_adjust(top=0.9)
    count= len(keylist)
    ncol = int(count/int((count/5+1))+1)
    ax[0].legend(loc='lower left', bbox_to_anchor=(0,1.1,1,1.4),mode="expand", ncol=ncol)
    plt.show()