import numpy as np
import lmfit
from scipy.interpolate import interp1d




def ensure_numpy_array(arr):
    if not isinstance(arr, np.ndarray):
        arr = np.array(arr)
    return arr


def num_deriv(x,y,w=1):
    x = ensure_numpy_array(x)
    y = ensure_numpy_array(y)
    y_deriv = (y[w:]-y[:-w])/(x[w:]-x[:-w])
    x_deriv = (x[w:]+x[:-w])/2
    return x_deriv, y_deriv

def sliding_average(x,y,w=2):
    x = ensure_numpy_array(x)
    y = ensure_numpy_array(y)
    conv = np.ones(w)/w
    return np.convolve(x, conv, mode='valid'),np.convolve(y, conv, mode='valid')

def get_x_filter(x, x_start, x_end):
    x = ensure_numpy_array(x)
    return np.logical_and(x_start < x, x < x_end)


def get_num_extrema(x,y,with_deriv=True, w_deriv=10, w_avg = 10):
    """
    returns viable,extremas,x_deriv,y_deriv
    viable indicates if the operation can even be done
    extremas is an array containing the zero crossings of the derivative
    if wtih_deriv true then it also returns x_deriv and y_deriv
    """

    if(len(x)<=w_deriv+ w_avg) :
        if with_deriv:
            return False,[],[],[]
        return False,[]
    x_deriv,y_deriv = num_deriv(x,y,w=w_deriv)
    x_deriv,y_deriv = sliding_average(x_deriv,y_deriv,w=w_avg)
    zero_crossings = interpolate_zero_crossings(x_deriv,y_deriv)
    if with_deriv :
        return True,zero_crossings,x_deriv,y_deriv
    return True,zero_crossings

def interpolate_zero_crossings(x, y):
    # Find indices where y changes sign (zero crossings)
    zero_crossings_indices = np.where(np.diff(np.sign(y)))[0]
    
    # Interpolate to find x values where y crosses zero
    x_zero_crossings = []
    for idx in zero_crossings_indices:
        interp_func = interp1d([y[idx], y[idx + 1]], [x[idx], x[idx + 1]])
        x_zero_crossings.append(interp_func(0))
    
    return x_zero_crossings



def fit_linear_model(x_data, y_data):
    """
    returns fitted intercept and slope
    """

    # Create a Model object from the linear model function
    model = lmfit.models.LinearModel()

    # Define initial parameter guesses
    params = model.guess(y_data,x=x_data)

    # Perform the fit
    result = model.fit(y_data, params, x=x_data)

    # Extract intercept and slope from the fit result
    intercept = result.params['intercept'].value
    slope = result.params['slope'].value

    d_intercept = result.params['intercept'].stderr
    d_slope = result.params['slope'].stderr

    return intercept, slope,d_intercept,d_slope