from csv_handling import *
import matplotlib.pyplot as plt
from numerical_math import *

def plotdata(x,y,label=None,zorder=1):
    return plt.plot(x,y,label=label,marker='o', linestyle='-', linewidth=0.8,markersize=3,zorder=zorder)

def characterise_plot_transistor_current(data,label,show_lines=True):
    x,y = data[0],data[1]
    if not show_lines:
        plotdata(x,y,label=label)
        return

    index_maxima=np.argmax(y)
    label_char = f"{label}, maxima: {y[index_maxima]} at {x[index_maxima]}"

    line=plotdata(x,y,label=label_char,zorder=3)
    x_deriv,y_deriv = num_deriv(x,y,w=1)

    #tries to find the index where the derivative noticably dips
    bool_arr=y_deriv>np.max(y_deriv)*2/3
    index = np.argmin(bool_arr)
    if bool_arr[index]:
        index=-1
    used_x_cut= x_deriv[index]

    f1 = get_x_filter(x,np.min(x),used_x_cut)
    inter,s,d_inter,d_s = fit_linear_model(x[f1],y[f1])
    plt.plot(x[f1],x[f1]*s+inter,label=f"{label} slope: {s} +- {d_s/s*100}%")
    used_x = x[index_maxima:]
    if len(used_x)>1:
        plt.axvline(x=used_x_cut, color=line[0].get_color(), linestyle='--')
        inter,s ,d_inter,d_s= fit_linear_model(used_x,y[index_maxima:])
        plt.plot(used_x,used_x*s+inter,label=f"{label} slope: {s}+- {d_s/s*100}%")


do_fits=True

data1 = get_data_fromsearchkey("I_BC_short",dtype="float")
data2 = get_data_fromsearchkey("I_BC_resistor",dtype="float")
characterise_plot_transistor_current(data1,"collector to +",show_lines=do_fits)
characterise_plot_transistor_current(data2,"collector to 33 Ohm",show_lines=do_fits)


plt.xlabel("base current(A)")
plt.ylabel("collector current(A)")
plt.title("current base-collector plot")
plt.legend()
plt.show()