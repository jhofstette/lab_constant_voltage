from csv_handling import *
from utils import *
import matplotlib.pyplot as plt
key= "source_150_Zener_50"
data = get_data_fromsearchkey(key,dtype="float")

volt= data[0]
ripple= data[1]
current=data[2]

fig,ax= plt.subplots(2,1,sharex=True)
ax[1].set_xlabel("Voltage (V)")
ax[0].set_ylabel("Current (A)")
ax[1].set_ylabel("Voltage (V)")
ax[0].set_title("measured Current")
ax[1].set_title("Ripple Voltage")


plotdata(ax[0],volt,current,label=key)
plotdata(ax[1],volt,ripple,label=key)

plt.legend()
plt.show()